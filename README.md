# frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

The url of the api is set to ```http://localhost:8080```, to change this you should change the next line of ```main.js```:

```
Vue.prototype.$apiurl = 'http://localhost:8080'
```

Set the variable to the url you want to use.

To login for the first time

```
Name: User 1
Password: 12345678
```